#!/usr/bin/make -f

DRAFT := draft-ietf-lamps-header-protection
OUTPUTS = $(DRAFT).xml $(DRAFT).txt $(DRAFT).html

TEST_VECTORS = $(shell ./generate-test-vectors list)

# Programs
KRAMDOWN := kramdown-rfc2629
XML2RFC := xml2rfc

# Targets
all: $(OUTPUTS)

%.xmlv2: %.mkd $(TEST_VECTORS)
	$(KRAMDOWN) --v3 $< > $@.tmp
	mv $@.tmp $@

%.xml: %.xmlv2
	$(XML2RFC) --v2v3 $< --out $@

%.txt: %.xml
	$(XML2RFC) --v3 --text $< --out $@

%.html: %.xml
	$(XML2RFC) --v3 --html $< --out $@

alice.both.crt: alice.sign.crt alice.encrypt.crt
	cat $^ > $@

$(TEST_VECTORS): generate-test-vectors alice.both.crt
	./generate-test-vectors

clean:
	rm -f $(OUTPUTS) $(DRAFT).xmlv2 metadata.min.js *.tmp alice.both.crt

check:
	mypy --strict generate-test-vectors

.PHONY: clean all check
