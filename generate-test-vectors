#!/usr/bin/env python3
'''Deterministically generate test vectors for draft-ietf-lamps-header-protection

This is unfinished, but the basic framework is in place.

still to do:

- S/MIME encryption+signing
- Header Confidentiality Policies, including legacy=true settings
- multipart/alternative bodies
- integration of test vectors into the draft itself
- PGP/MIME messages
- ensure that cryptographic measures are in fact deterministic

and maybe:

- S/MIME encrypt-only?
- Include CA certificate?
'''

import os
import uuid
import subprocess
from email.message import MIMEPart, Message
from email.policy import EmailPolicy
from email.utils import parsedate_to_datetime
from typing import Optional, List, Callable, Sequence
from cryptography import x509
from datetime import datetime, timedelta
from logging import warning
from hashlib import sha256
from sys import argv

sample_seed:int = 0

# see draft-autocrypt-lamps-protected-headers-03 #user-facing-headers:
user_facing_headers = ['subject',
                       'from',
                       'to',
                       'cc',
                       'date',
                       'reply-to',
                       'followup-to']

def generate_new_message_id() -> str:
    '''Generate a new random message ID

    In a real implementation, this should be pulled from a real source
    of randomness, like uuid.uuid4().

    In this implementation, we opt for something deterministic,
    because are trying to generate reproducible test vectors
    deterministicically.  it repeats each time.
    '''

    # selected randomly:
    draft_lamps_header_protection_namespace = uuid.UUID('db43545a-f0b3-46bb-8263-7a9be15fd480')
    seedstr:str = str(sample_seed)
    sample_seed += 1
    return str(uuid.uuid5(draft_lamps_header_protection_namespace, seedstr))

class Header():
    def __init__(self, name:str, val:str):
        self.name:str = name
        self.value:str = val

# Example Header Confidentiality Policies
class HeaderConfidentialityPolicy:
    def __init__(self, legacy:bool=False):
        self.legacy:bool = legacy
    @property
    def name(self) -> str:
        raise NotImplementedError('abstract base class HeaderConfidentialityPolicy')
    def policy(self, name:str, val_in:str) -> Optional[str]:
        raise NotImplementedError('unimplemented policy for HCP base class')
    def apply(self, hdrs:List[Header]) -> List[Header]:
        '''see use in #compose-wrapped-message and #compose-injected-headers'''
        newh:List[Header] = []
        for h in hdrs:
            newval = self.policy(h.name, h.value)
            if newval is not None:
                newh += [Header(h.name, newval)]
        return newh
    def __str__(self) -> str:
        if self.legacy:
            return f'{self.name} (legacy)'
        else:
            return self.name

class HCPMinimal(HeaderConfidentialityPolicy):
    @property
    def name(self) -> str:
        return 'hcp_minimal'
    def policy(self, name:str, val_in:str) -> Optional[str]:
        'defined in #minimal-hcp'
        if name.lower() == 'subject':
            return '[...]'
        else:
            return val_in

class HCPStrong(HeaderConfidentialityPolicy):
    @property
    def name(self) -> str:
        return 'hcp_strong'
    def policy(self, name:str, val_in:str) -> Optional[str]:
        'defined in #strong-hcp'
        if name.lower() in ['from', 'to', 'cc', 'date']:
            return val_in
        elif name.lower() == 'subject':
            return '[...]'
        elif name.lower() == 'message-id':
            return generate_new_message_id()
        return None


def get_date(headers:List[Header]) -> datetime:
    for h in headers:
        if h.name.lower() == 'date':
            return parsedate_to_datetime(h.value)
    raise ValueError('Expected Date: header, but it was not present')

class Cert():
    def __init__(self) -> None:
        pass
class Key():
    def __init__(self) -> None:
        super().__init__()
        pass

class X509Cert(Cert):
    def __init__(self, certfname:str) -> None:
        with open(certfname, 'rb') as certf:
            self._x509 = x509.load_pem_x509_certificate(certf.read())
    
class X509(Key):
    def __init__(self, keyfname:str, certfname:str) -> None:
        self.cert = X509Cert(certfname)
        self._cert_fname = certfname
        self._key_fname = keyfname
        # FIXME: verify that key corresponds to keyfname
    def _sign(self, data:bytes, when:datetime, subcmd:str) -> bytes:
        inp, outp = os.pipe()
        with open(outp, 'wb') as outf:
            outf.write(data)
        out:subprocess.CompletedProcess[bytes]
        utcwhen:datetime = datetime.utcfromtimestamp(when.timestamp())
        env = {'TZ': 'UTC',
               'FAKETIME': utcwhen.strftime('%Y-%m-%d %H:%M:%S'),
               'FAKERANDOM_SEED': '0x0000000000000000',
               }
        cmd = ['certtool', subcmd,
               '--p7-time', '--outraw',
               '--hash', 'SHA256', # FIXME: can we pick a new hash? this is bound to micalg, below
               '--load-privkey', self._key_fname,
               '--load-certificate', self._cert_fname]
        out = subprocess.run(cmd, stdin=inp, capture_output=True, env=env)
        if out.returncode != 0:
            warning(cmd)
            warning(out.stderr)
            raise Exception(f'certtool {subcmd} failed with {out.returncode}')
        return out.stdout

    
class CryptoOp():
    def __init__(self) -> None:
        pass
    def apply(self, part:MIMEPart, when:datetime) -> MIMEPart:
        raise NotImplementedError('undefined crypto operation')
    @property
    def confidentiality(self) -> bool:
        return False

class SMIMESignPKCS7(CryptoOp):
    # FIXME: figure out how to do signatures from multiple keys. for now, just one.
    def __init__(self, key:X509) -> None:
        super().__init__()
        self.key:X509 = key
    def apply(self, part:MIMEPart, when:datetime) -> MIMEPart:
        txt:str = part.as_string(policy=EmailPolicy(linesep='\r\n'))
        out = self.key._sign(txt.encode(), when, '--p7-sign')
        msg = MIMEPart()
        msg.set_content(out, 'application', 'pkcs7-mime')
        msg.set_param('name','smime.p7m')
        msg.set_param('smime-type','signed-data')
        return msg

class SMIMESignMultipart(CryptoOp):
    # FIXME: figure out how to do signatures from multiple keys. for now, just one.
    def __init__(self, key:X509) -> None:
        super().__init__()
        self.key = key
    def apply(self, part:MIMEPart, when:datetime) -> MIMEPart:
        txt:str = part.as_string(policy=EmailPolicy(linesep='\r\n'))
        out = self.key._sign(txt.encode(), when, '--p7-detached-sign')
        sigpart = MIMEPart(EmailPolicy(max_line_length=64))
        sigpart.set_content(out, 'application', 'pkcs7-signature')
        sigpart.set_param('name','smime.p7s')
        wrapper = MIMEPart()
        wrapper.set_type('multipart/signed')
        wrapper.set_param('protocol', sigpart.get_content_type())
        wrapper.set_boundary(sha256(bytes(part)).hexdigest()[:3])
        # FIXME: this is bound to SHA256 above.  need to figure out how to be more flexible.
        wrapper.set_param('micalg', 'sha-256')
        wrapper.attach(part)
        wrapper.attach(sigpart)
        return wrapper

class SMIMEEncrypt(CryptoOp):
    def __init__(self, certs:List[Cert]) -> None:
        super().__init__()
        self.certs:List[Cert] = certs
    @property
    def confidentiality(self) -> bool:
        return True
    def apply(self, part:MIMEPart, when:datetime) -> MIMEPart:
        raise NotImplementedError('still need to implement SMIMEEncrypt')

# FIXME: add RFC 3156 PGP/MIME CryptoOps: PGPSign and PGPSignEncrypt (and maybe signature-less PGPEncrypt)

MessageCrypto = Callable[[MIMEPart, List[Header], Sequence[CryptoOp], Optional[HeaderConfidentialityPolicy]], Message]
        
def nocrypto(origbody:MIMEPart,
             origheaders:List[Header],
             crypto:Sequence[CryptoOp]=[],
             hcp:Optional[HeaderConfidentialityPolicy]=None) -> MIMEPart:
    '''compose a message, ignoring all Cryptographic instructions'''
    if crypto != []:
        raise NotImplementedError('asking for crypto operations from nocrypto()')
    if hcp is not None:
        raise NotImplementedError('asking for Header Confidentiality Policy  from nocrypto()')
    for h in origheaders:
        origbody[h.name] = h.value
    return origbody

def legacy(origbody:MIMEPart,
           origheaders:List[Header],
           crypto:Sequence[CryptoOp],
           hcp:Optional[HeaderConfidentialityPolicy]=None) -> Message:
    '''Original transformation, with no header protection

    from #compose-legacy
    '''
    when = get_date(origheaders)
    
    if hcp is not None:
        raise NotImplementedError('asking for Header Confidentiality Policy  from nocrypto()')
    for op in crypto:
        origbody = op.apply(origbody, when)
    return nocrypto(origbody, origheaders)


def wrapped_message(origbody:MIMEPart,
                    origheaders:List[Header],
                    crypto:Sequence[CryptoOp],
                    hcp:Optional[HeaderConfidentialityPolicy]) -> Message:
    '''Use the Wrapped Message construct to generate the message with header protection

    from #compose-wrapped-message
    '''
    when = get_date(origheaders)
    wrapper = MIMEPart()
    wrapper.set_payload(str(nocrypto(origbody, origheaders)))
    # FIXME: not testing for need for message/global:
    wrapper.set_type('message/rfc822')
    wrapper.set_param('forwarded', 'no')

    for op in crypto:
        wrapper = op.apply(wrapper, when)

    if list(filter(lambda op: op.confidentiality, crypto)) and hcp is not None:
        origheaders = hcp.apply(origheaders)
    return nocrypto(wrapper, origheaders)
        

def injected_headers(origbody:MIMEPart,
                     origheaders:List[Header],
                     crypto:Sequence[CryptoOp],
                     hcp:Optional[HeaderConfidentialityPolicy]) -> Message:
    '''Use the Wrapped Message construct to generate the message with header protection

    from #compose-injected-headers
    '''
    when = get_date(origheaders)
    if hcp is not None and hcp.legacy:
        lines = []
        if list(filter(lambda op: op.confidentiality, crypto)):
            for h in origheaders:
                if h.name.lower() in user_facing_headers:
                    if hcp.policy(h.name,h.value) != h.value:
                        # FIXME: ensure that value isn't spread across multiple lines?
                        # FIXME: un-RFC2047-encode values?
                        lines += [f'{h.name}: {h.value}']
        if lines:
            legacydisplay = MIMEPart()
            legacydisplay.set_type('text/plain')
            legacydisplay.set_param('protected-headers', 'v1')
            legacydisplay.set_content('\n'.join(lines))
            wrapper = MIMEPart()
            wrapper.set_type('multipart/mixed')
            wrapper.attach(legacydisplay)
            wrapper.attach(origbody)
            origbody = wrapper

    payload = nocrypto(origbody, origheaders)
    payload.set_param('protected-headers', 'v1')
    for op in crypto:
        payload = op.apply(origbody, when)

    if list(filter(lambda op: op.confidentiality, crypto)) and hcp is not None:
        origheaders = hcp.apply(origheaders)
    return nocrypto(payload, origheaders)

BodyCreator = Callable[[str], MIMEPart]

def plain_text_body(name:str) -> MIMEPart:
    ret = MIMEPart()
    ret.set_type('text/plain')
    ret.set_content(f'This is the {name} message.')
    return ret    

class Headers:
    def __init__(self, subject:str):
        self.subject = subject
    def hdr(self, name:str, when:datetime) -> List[Header]:
        return [
            Header('Subject', self.subject),
            Header('Message-ID', f'<{name}@lamps-header-protection.example>'),
            Header('From', 'Alice <alice@smime.example>'),
            Header('To', 'Bob <bob@smime.example>'),
            Header('Date', when.strftime('%a, %d %b %Y %T %z').strip())
        ]

class SampleMessage():
    def __init__(self,
                 name:str,
                 body_creator:BodyCreator,
                 header_creator:Headers,
                 when:datetime,
                 crypto:MessageCrypto=nocrypto,
                 ops:Sequence[CryptoOp]=[],
                 hcp:Optional[HeaderConfidentialityPolicy]=None) -> None:
        self.name = name
        self.body_creator = body_creator
        self.headers = header_creator
        self.when = when
        self.crypto = crypto
        self.ops = ops
        self.hcp = hcp
    def write(self) -> None:
        msg = self.crypto(self.body_creator(self.name), self.headers.hdr(self.name, self.when), self.ops, self.hcp)
        
        with open(f'{self.name}.eml', 'wb') as outf:
            outf.write(bytes(msg))

alice = X509('alice.sign.key', 'alice.both.crt')
pkcs7sign = [SMIMESignPKCS7(alice)]
multipart = [SMIMESignMultipart(alice)]

base_time:datetime = datetime.fromisoformat('2021-02-20T10:00:02-05:00')

def t(minutes:int) -> datetime:
    return base_time + timedelta(minutes=minutes)

msgs:List[SampleMessage] = [
    SampleMessage('no-crypto', plain_text_body, Headers('The FooCorp Contract'), t(0)),
    SampleMessage('smime-one-part', plain_text_body, Headers('The FooCorp Contract'),
                  t(1), legacy, pkcs7sign),
    SampleMessage('smime-multipart', plain_text_body, Headers('The FooCorp Contract'),
                  t(2), legacy, multipart),
    SampleMessage('smime-one-part-wrapped', plain_text_body, Headers('The FooCorp Contract'),
                  t(3), wrapped_message, pkcs7sign),
    SampleMessage('smime-multipart-wrapped', plain_text_body, Headers('The FooCorp Contract'),
                  t(4), wrapped_message, multipart),
    SampleMessage('smime-one-part-injected', plain_text_body, Headers('The FooCorp Contract'),
                  t(5), wrapped_message, pkcs7sign),
    SampleMessage('smime-multipart-injected', plain_text_body, Headers('The FooCorp Contract'),
                  t(6), wrapped_message, multipart),
]


hcps = [
    None,
    HCPMinimal(),
    HCPMinimal(legacy=True),
    HCPStrong(),
    HCPStrong(legacy=True),
]

if len(argv) > 1 and argv[1] == 'list':
    for msg in msgs:
        print(f'{msg.name}.eml')
else:
    for msg in msgs:
        msg.write()
